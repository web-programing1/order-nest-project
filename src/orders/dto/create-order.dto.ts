import { IsNotEmpty, IsNumber } from "class-validator";

class CreatedOrderItemDto {
  @IsNotEmpty()
  @IsNumber()
  productId: number;

  @IsNotEmpty()
  @IsNumber()
  amount: number;
}

export class CreateOrderDto {
  @IsNotEmpty()
  @IsNumber()
  customerId: number;

  @IsNotEmpty()
  orderItems: CreatedOrderItemDto[];
}
