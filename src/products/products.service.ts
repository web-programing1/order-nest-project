import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Product)
    private productsRepository: Repository<Product>,
  ) {}

  create(createProductDto: CreateProductDto) {
    return this.productsRepository.save(createProductDto);
  }

  findAll() {
    return this.productsRepository.find();
  }

  async findOne(id: number) {
    const product = await this.productsRepository.findOne({
      where: { id: id },
      relations: ['orderItems'],
    });
    if (!product) {
      return new NotFoundException();
    }
    return product;
  }

  update(id: number, updateProductDto: UpdateProductDto) {
    this.productsRepository.update(id, updateProductDto);
    return this.productsRepository.findOne({ where: { id: id } });
  }

  async remove(id: number) {
    const customer = await this.productsRepository.findOne({
      where: { id: id },
      relations: ['orderItems'],
    });
    if (!customer) {
      return new NotFoundException();
    }
    return this.productsRepository.softRemove({ id: id });
  }
}
